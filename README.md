Semestrální práce MVI - Rozpoznávání žánru písničky na základě jejího textu.

Jako téma mé semestrální práce jsem si vybral rozpoznávání žánru písniček podle jejich textu.
Rozhodl jsem se provádět predikci pouze mezi 5 žánry: pop, rock, country, electronic a hiphop.
Cílem bylo porovnat 4 modely: DNN a XGB využívající tf-idf reprezentaci a RNN a CNN využívající
word embedding. Dalším cílem bylo zjistit vliv technik balancování dat, odstranění zbytečných
slov(stopwords) a stemming na výkon modelu.

V souboru data_preparation.ipynb se provádí předzpracování dat ze souborů train.csv a test.csv,
které jsou k dispozizi na https://www.kaggle.com/mateibejan/multilingual-lyrics-for-genre-classification.
Pročištěná data jsou uložena do souboru clean_data.csv, který je k dispozici v repozitáři. V souboru
DNN_XGB.ipynb se používají pouze tyto již pročištěná data na trénování modelů využívajících tf-idf
reprezentaci DNN a XGB. V souboru RNN_CNN.ipynb jsou tyto data použita na trénování modelů využívajících
word embedding RNN a CNN. V souboru functions.py jsou definovány funkce používané v souborech DNN_XGB.ipynb
a RNN_CNN.ipynb.
